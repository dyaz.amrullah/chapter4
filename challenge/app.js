const express = require('express');
const app = express();
const port = 3000;

const morgan = require('morgan');
app.use(morgan('dev'));
app.use(express.json());

const router = require('./routes');
app.use(router);

// 404 handler
app.use((req, res, next) => {
  return res.status(404).json({ message: `can't find ${req.url}` });
});

// 500 handler
app.use((err, req, res, next) => {
  return res.status(500).json({ message: err.message });
});

app.listen(port, () => console.log(`listening on port ${port}!`))