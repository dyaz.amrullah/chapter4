Projek Challenge Chapter 4 Binar Academy

Tasks     : 1. Merancang API yang dinamis 
            2. Menggunakan JSON untuk menampilkan data ke client nya
            3. Dapat melakukan CRUD dengan RESTful API menggunakan express.js
            4. Konfigurasi perancangan database menggunakan ORM

Pemilik   : Dyaz Amrullah
Kelas     : Back-End Javascript
Challenge : Chapter 04